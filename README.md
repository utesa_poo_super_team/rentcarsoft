# RentCarSoft #

### What is this repository for? ###

Este es un proyecto desarrollado para implementar lo aprendido en la materia de Programacion Orientada a Objetos de UTESA 

---
#### Integrantes ###

* Escarlin Peña 2-13-1485
* Ronald Mármol 2-09-2467
* Tamar Colón 2-13-1946

---
###### Informaciones.

** Configuracion del netbeans.**
  
* Dentro de la carpeta del proyecto se encuentra una carpeta que se llama Libs la cual contiene las librerias <br>
    utilizadas en el sistema por favor agregarlas al proyecto dandole click derecho encima del proyecto > librerias > agregar Jar.

* para los componentes de las fecha agregal al proyecto la libreria jcalendar.1.4 y agregar a la paleta de componentes.

- - - 
La clase de conexion a la db esta en la paqueteria:
```
edu.utesa.poo.rentcarsoft.utils
  DBM.java 
```

##### Script de Creación en la db
```
CREATE DATABASE IF NOT EXISTS rentcarsoft;

CREATE TABLE rentcarsoft.customer
(
  name         VARCHAR(200) NULL,
  code         INT          NOT NULL AUTO_INCREMENT
    PRIMARY KEY,
  email        VARCHAR(200) NULL,
  gender       VARCHAR(30)  NULL,
  occupation   VARCHAR(200) NULL,
  idcard       VARCHAR(200) NULL,
  address      VARCHAR(500) NULL,
  phone_number VARCHAR(200) NULL
);

CREATE TABLE rentcarsoft.office
(
  name    VARCHAR(200) NULL,
  address TEXT         NULL,
  city    VARCHAR(50)  NULL
);

CREATE TABLE rentcarsoft.rental
(
  id             MEDIUMTEXT   NULL,
  pickup_date    DATETIME     NULL,
  return_date    DATETIME     NULL,
  idVehicle      INT          NULL,
  idPayment      INT          NULL,
  description    VARCHAR(200) NULL,
  odometer_start DECIMAL(18)  NULL,
  odometer_end   DECIMAL(18)  NULL,
  gas_tank_level DECIMAL(18)  NULL,
  insurance      VARCHAR(200) NULL,
  rental_type    VARCHAR(200) NULL
);

CREATE TABLE rentcarsoft.payment
(
  id           INT          NULL,
  date         DATETIME     NULL,
  amount       DECIMAL(18)  NULL,
  payment_type VARCHAR(200) NULL,
  estatus      VARCHAR(200) NULL
);

CREATE TABLE rentcarsoft.reservation
(
  code        INT         NOT NULL AUTO_INCREMENT
    PRIMARY KEY,
  pickup_date DATETIME    NULL,
  return_date DATETIME    NULL,
  advance     DECIMAL(18) NULL,
  estatus     VARCHAR(50) NULL
);

CREATE TABLE rentcarsoft.users
(
  code         INT          NOT NULL AUTO_INCREMENT
    PRIMARY KEY,
  name         VARCHAR(200) NULL,
  username     VARCHAR(200) NULL,
  password     VARCHAR(200) NULL,
  address      VARCHAR(500) NULL,
  idcard       VARCHAR(200) NULL,
  email        VARCHAR(200) NULL,
  gender       VARCHAR(200) NULL,
  phone_number VARCHAR(200) NULL,
  user_type    VARCHAR(200) NULL
);

CREATE TABLE rentcarsoft.vehicle
(
  car_id          INT          NOT NULL AUTO_INCREMENT
    PRIMARY KEY,
  brand           VARCHAR(200) NULL,
  color           VARCHAR(200) NULL,
  price           DECIMAL(18)  NULL,
  model_name      VARCHAR(200) NULL,
  year            INT          NULL,
  chassis         VARCHAR(500) NULL,
  passager_number INT          NULL,
  vehicle_type    VARCHAR(500) NULL
);



```
##### Script de Creación de Usuario para la DB.

```
CREATE USER 'rentcar'@'localhost'
  IDENTIFIED BY 'rent123car';
GRANT USAGE ON *.* TO 'rentcar'@'localhost';
GRANT SELECT, EXECUTE, SHOW VIEW, ALTER, ALTER ROUTINE, CREATE, 
CREATE ROUTINE, CREATE TEMPORARY TABLES, CREATE VIEW, DELETE, DROP, 
EVENT, INDEX, INSERT, REFERENCES, TRIGGER, UPDATE, LOCK TABLES ON `rentcarsoft`.* TO 'rentcar'@'localhost'
WITH GRANT OPTION;
FLUSH PRIVILEGES;

```

##### Mockup de las ventanas realizadas

![18012797_1425720260783583_1347448581_o.jpg](https://bitbucket.org/repo/gob4xx/images/2732451664-18012797_1425720260783583_1347448581_o.jpg)

![18053466_1425720257450250_65661265_o.jpg](https://bitbucket.org/repo/gob4xx/images/1441254983-18053466_1425720257450250_65661265_o.jpg)
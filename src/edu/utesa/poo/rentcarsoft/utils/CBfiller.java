/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.utesa.poo.rentcarsoft.utils;

import javax.swing.JComboBox;

/**
 *
 * @author ronald
 */
public class CBfiller {

    private static CBfiller instance = null;

    private CBfiller() {
    }

    public static synchronized CBfiller get() {
        if (instance == null) {
            instance = new CBfiller();
        }
        return instance;
    }

    public static JComboBox fillGenre(JComboBox comboBox) {
        comboBox.removeAllItems();
        for (String genre : Constants.get().getGenres()) {
            comboBox.addItem(genre);
        }
        return comboBox;
    }

    public static JComboBox fillUserType(JComboBox comboBox) {
        comboBox.removeAllItems();
        for (String userType : Constants.get().getUserType()) {
            comboBox.addItem(userType.toString());
        }
        return comboBox;
    }

    public static JComboBox fillPaymentType(JComboBox comboBox) {
        comboBox.removeAllItems();
        for (String paymentType : Constants.get().getPaymentType()) {
            comboBox.addItem(paymentType.toString());
        }
        return comboBox;
    }

    public static JComboBox fillVehicleType(JComboBox comboBox) {
        comboBox.removeAllItems();
        for (String vehicleType : Constants.get().getVehicleType()) {
            comboBox.addItem(vehicleType.toString());
        }
        return comboBox;
    }

    public static JComboBox fillRentalType(JComboBox comboBox) {
        comboBox.removeAllItems();
        for (String rentalType : Constants.get().getRentalType()) {
            comboBox.addItem(rentalType.toString());
        }
        return comboBox;
    }

}

package edu.utesa.poo.rentcarsoft.utils;

import edu.utesa.poo.rentcarsoft.models.domain.control.Customer;
import edu.utesa.poo.rentcarsoft.models.domain.payment.Payment;
import edu.utesa.poo.rentcarsoft.models.domain.rent.Rental;
import edu.utesa.poo.rentcarsoft.models.domain.security.User;
import edu.utesa.poo.rentcarsoft.models.domain.vehicle.Vehicle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author ronald
 */
public class DBQueries {

    private static final String CUSTOMER_TABLE = "  customer";
    private static final String RENTAL_TABLE = "  rental";
    private static final String USERS_TABLE = " users";
    private static final String VEHICLE_TABLE = " vehicle";
    private static final String PAYMENT_TABLE = " payment";
    private static final String FIND_VEHICLE_QUERY = "select car_id,brand,color,price,model_name,year,chassis,passager_number from " + VEHICLE_TABLE + " where car_id = '";
    private static final String FIND_PAYMENT_QUERY = "select id,date,amount,payment_type,estatus from " + PAYMENT_TABLE + " where id = '";
    private static final String FIND_USER_QUERY = "select code ,name ,username ,password ,address ,idcard ,email ,gender ,phone_number ,user_type from " + USERS_TABLE + " where code = '";
    private static final String FIND_RENTAL_QUERY = "select id, pickup_date, return_date, idVehicle, idPayment, description, odometer_start, "
            + "odometer_end, gas_tank_level, insurance, rental_type from " + RENTAL_TABLE + " where id = '";

    DBM dbm = new DBM();

    public Boolean doLogin(Connection connection, String username, String password) {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Boolean exist = false;
        try {
            connection = dbm.getConnection();
            preparedStatement = connection.prepareStatement("select name from " + USERS_TABLE + " where  " + " username = '" + username
                    + "' and " + "password ='" + password + "';");
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                if (resultSet.getString("name") != null && !resultSet.getString("name").isEmpty()) {
                    exist = true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return exist;
    }

    public ResultSet findAll(Connection connection, String query) {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            if (connection == null) {
                connection = dbm.getConnection();
            }
            preparedStatement = connection.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            if (!resultSet.isBeforeFirst()) {
                System.out.println("No data");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return resultSet;
    }

    public List<Rental> findAllRental() {
        PreparedStatement preparedStatement = null;
        ResultSet rsRental = null;
        Connection connection = dbm.getConnection();
        List<Rental> rentals = new ArrayList<>();
        Rental rental;
        String query = "Select *  from " + RENTAL_TABLE + ";";
        System.out.println(query);
        try {
            preparedStatement = connection.prepareStatement(query);
            rsRental = preparedStatement.executeQuery();
            while (rsRental.next()) {
                rental = new Rental();
                rental.setId(Long.parseLong(rsRental.getString("id")));
                rental.setPickupDate(rsRental.getDate("pickup_date"));
                rental.setReturnDate(rsRental.getDate("return_date"));
                if ("".equals(rsRental.getInt("idVehicle"))) {
                    rental.setVehicle(new Vehicle());
                } else {
                    System.out.println(FIND_VEHICLE_QUERY + rsRental.getInt("idVehicle") + "';");
                    ResultSet rsVehicle = this.find(dbm.getConnection(), FIND_VEHICLE_QUERY + rsRental.getInt("idVehicle") + "';");
                    rsVehicle.next();
                    Vehicle vehicle = new Vehicle();
                    vehicle.setCarID(Long.parseLong(rsVehicle.getString("car_id")));
                    vehicle.setBrand(rsVehicle.getString("brand"));
                    vehicle.setColor(rsVehicle.getString("color"));
                    vehicle.setPrice(rsVehicle.getBigDecimal("price"));
                    vehicle.setModelName(rsVehicle.getString("model_name"));
                    vehicle.setYear(Integer.parseInt(rsVehicle.getString("year")));
                    vehicle.setChassis(rsVehicle.getString("chassis"));
                    vehicle.setPassangerNumbers(Integer.parseInt(rsVehicle.getString("passager_number")));
                    rental.setVehicle(vehicle);
                }
                if ("".equals(rsRental.getInt("idPayment"))) {
                    rental.setPayment(new Payment());
                } else {
                    System.out.println(FIND_PAYMENT_QUERY + rsRental.getInt("idPayment") + "';");
                    ResultSet rsPayment = this.find(dbm.getConnection(), FIND_PAYMENT_QUERY + rsRental.getInt("idPayment") + "';");
                    rsPayment.next();
                    Payment payment = new Payment();
                    payment.setId(Long.parseLong(rsPayment.getString("id")));
                    payment.setDate(rsPayment.getDate("date"));
                    payment.setAmount(rsPayment.getBigDecimal("amount"));
                    payment.setPaymentType(rsPayment.getString("payment_type"));
                    payment.setEstatus(rsPayment.getString("estatus"));
                    rental.setPayment(payment);
                }
                rental.setDescription(rsRental.getString("description"));
                rental.setOdometerStart(Long.parseLong(rsRental.getString("odometer_start")));
                rental.setOdometerEnd(Long.parseLong(rsRental.getString("odometer_end")));
                rental.setInsurance(rsRental.getString("insurance"));
                rental.setGasTankLevel(Long.parseLong(rsRental.getString("gas_tank_level")));
                rental.setRentalType(rsRental.getString("rental_type"));
                rentals.add(rental);
            }
            preparedStatement.close();
            dbm.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            JOptionPane.showMessageDialog(null, "Error al consultar", "Error", JOptionPane.ERROR_MESSAGE);
        }
        return rentals;
    }

    public List<User> findAllUsers() {
        PreparedStatement preparedStatement = null;
        ResultSet rsUsers = null;
        Connection connection = dbm.getConnection();
        List<User> users = new ArrayList<>();
        User user;
        String query = "Select *  from " + USERS_TABLE + ";";
        System.out.println(query);
        try {
            preparedStatement = connection.prepareStatement(query);
            rsUsers = preparedStatement.executeQuery();
            while (rsUsers.next()) {
                user = new User();
                user.setCode(Long.parseLong(rsUsers.getString("code")));
                user.setName(rsUsers.getString("name"));
                user.setUserName(rsUsers.getString("username"));
                user.setPassWord(rsUsers.getString("password"));
                user.setAddress(rsUsers.getString("address"));
                user.setIdCard(rsUsers.getString("idcard"));
                user.setEmail(rsUsers.getString("email"));
                user.setGender(rsUsers.getString("gender"));
                // user.setGender(Genre.valueOf(rsUsers.getString("gender")));
                user.setPhoneNumber(rsUsers.getString("phone_number"));
                user.setUserType(rsUsers.getString("user_type"));
                users.add(user);
            }
            preparedStatement.close();
            dbm.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            JOptionPane.showMessageDialog(null, "Error al consultar", "Error", JOptionPane.ERROR_MESSAGE);
        }
        return users;
    }

    public List<Vehicle> findAllVehicle() {
        PreparedStatement preparedStatement = null;
        ResultSet rsVehicle = null;
        Connection connection = dbm.getConnection();
        List<Vehicle> vehicles = new ArrayList<>();
        Vehicle vehicle;
        String query = "Select *  from " + VEHICLE_TABLE + ";";
        System.out.println(query);
        try {
            preparedStatement = connection.prepareStatement(query);
            rsVehicle = preparedStatement.executeQuery();
            while (rsVehicle.next()) {
                vehicle = new Vehicle();
                vehicle.setCarID(Long.parseLong(rsVehicle.getString("car_id")));
                vehicle.setBrand(rsVehicle.getString("brand"));
                vehicle.setVehicleType(rsVehicle.getString("vehicle_type"));
                vehicle.setColor(rsVehicle.getString("color"));
                vehicle.setPrice(rsVehicle.getBigDecimal("price"));
                vehicle.setChassis(rsVehicle.getString("chassis"));
                vehicle.setModelName(rsVehicle.getString("model_name"));
                vehicle.setYear(rsVehicle.getInt("year"));
                vehicle.setPassangerNumbers(rsVehicle.getInt("passager_number"));
                vehicles.add(vehicle);
            }
            preparedStatement.close();
            dbm.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            JOptionPane.showMessageDialog(null, "Error al consultar", "Error", JOptionPane.ERROR_MESSAGE);
        }
        return vehicles;
    }

    public List<Payment> findAllPayment() {
        PreparedStatement preparedStatement = null;
        ResultSet rsVehicle = null;
        Connection connection = dbm.getConnection();
        List<Payment> payments = new ArrayList<>();
        Payment payment;
        String query = "Select *  from " + PAYMENT_TABLE + ";";
        System.out.println(query);
        try {
            preparedStatement = connection.prepareStatement(query);
            rsVehicle = preparedStatement.executeQuery();
            while (rsVehicle.next()) {
                payment = new Payment();
                payment.setId(Long.parseLong(rsVehicle.getString("id")));
                payment.setDate(rsVehicle.getDate("date"));
                payment.setAmount(rsVehicle.getBigDecimal("amount"));
                payment.setPaymentType(rsVehicle.getString("payment_type"));
                payment.setEstatus(rsVehicle.getString("estatus"));
                payments.add(payment);
            }
            preparedStatement.close();
            dbm.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            JOptionPane.showMessageDialog(null, "Error al consultar", "Error", JOptionPane.ERROR_MESSAGE);
        }
        return payments;
    }

    public ResultSet find(Connection connection, String query) {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = dbm.getConnection();
            preparedStatement = connection.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            if (!resultSet.isBeforeFirst()) {
                System.out.println("No data");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return resultSet;
    }

    public ResultSet findPaymentByID(Connection connection, Payment payment) {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = dbm.getConnection();
            preparedStatement = connection.prepareStatement(FIND_PAYMENT_QUERY + payment.getId() + "';");
            resultSet = preparedStatement.executeQuery();
            if (!resultSet.isBeforeFirst()) {
                System.out.println("No data");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return resultSet;
    }

    public ResultSet findVehicleByID(Connection connection, Vehicle vehicle) {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = dbm.getConnection();
            preparedStatement = connection.prepareStatement(FIND_VEHICLE_QUERY + vehicle.getCarID() + "';");
            resultSet = preparedStatement.executeQuery();
            if (!resultSet.isBeforeFirst()) {
                System.out.println("No data");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return resultSet;
    }

    public ResultSet findVehicleByID(Connection connection, String id) {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = dbm.getConnection();
            preparedStatement = connection.prepareStatement(FIND_VEHICLE_QUERY + id + "';");
            resultSet = preparedStatement.executeQuery();

            if (!resultSet.isBeforeFirst()) {
                System.out.println("No data");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return resultSet;
    }

    public Vehicle getVehicleByID(Connection connection, String id) {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Vehicle vehicle = null;
        try {
            connection = dbm.getConnection();
            preparedStatement = connection.prepareStatement(FIND_VEHICLE_QUERY + id + "';");
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                vehicle = new Vehicle();
                vehicle.setCarID(Long.parseLong(resultSet.getString("car_id")));
                vehicle.setBrand(resultSet.getString("brand"));
                vehicle.setColor(resultSet.getString("color"));
                vehicle.setPrice(resultSet.getBigDecimal("price"));
                vehicle.setModelName(resultSet.getString("model_name"));
                vehicle.setYear(Integer.parseInt(resultSet.getString("year")));
                vehicle.setChassis(resultSet.getString("chassis"));
                vehicle.setPassangerNumbers(Integer.parseInt(resultSet.getString("passager_number")));

            } else {
                vehicle = new Vehicle();
            }
            if (!resultSet.isBeforeFirst()) {
                System.out.println("No data");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return vehicle;
    }

    public Payment getPaymentByID(Connection connection, String id) {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Payment payment = null;
        try {
            connection = dbm.getConnection();
            preparedStatement = connection.prepareStatement(FIND_PAYMENT_QUERY + id + "';");
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                payment = new Payment();
                payment.setId(Long.parseLong(resultSet.getString("id")));
                payment.setDate(resultSet.getDate("date"));
                payment.setAmount(resultSet.getBigDecimal("amount"));
                payment.setPaymentType(resultSet.getString("payment_type"));
                payment.setEstatus(resultSet.getString("estatus"));

            } else {
                payment = new Payment();
            }
            if (!resultSet.isBeforeFirst()) {
                System.out.println("No data");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return payment;
    }

    public ResultSet findRentalByID(Connection connection, Rental rental) {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = dbm.getConnection();
            preparedStatement = connection.prepareStatement(FIND_RENTAL_QUERY + rental.getId() + "';");
            resultSet = preparedStatement.executeQuery();
            if (!resultSet.isBeforeFirst()) {
                System.out.println("No data");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return resultSet;
    }

    public ResultSet findUserByID(Connection connection, User user) {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = dbm.getConnection();
            preparedStatement = connection.prepareStatement(FIND_USER_QUERY + user.getCode() + "';");
            resultSet = preparedStatement.executeQuery();
            if (!resultSet.isBeforeFirst()) {
                System.out.println("No data");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return resultSet;
    }

    public Long findMaxVehicleCode(Connection connection) {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Long maxCode = 0l;
        try {
            connection = dbm.getConnection();
            preparedStatement = connection.prepareStatement("SELECT max(car_id) data FROM" + VEHICLE_TABLE);
            resultSet = preparedStatement.executeQuery();
            resultSet.next();
            if (resultSet.getString("data") != null && !resultSet.getString("data").isEmpty()) {
                maxCode = Long.parseLong(resultSet.getString("data"));
            } else {
                maxCode = 0l;
            }
            if (!resultSet.isBeforeFirst()) {
                System.out.println("No data");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return maxCode;
    }

    public Long findMaxRentalCode(Connection connection) {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Long maxCode = 0l;
        try {
            connection = dbm.getConnection();
            preparedStatement = connection.prepareStatement("SELECT max(id) data FROM" + RENTAL_TABLE);
            resultSet = preparedStatement.executeQuery();
            resultSet.next();
            if (resultSet.getString("data") != null && !resultSet.getString("data").isEmpty()) {
                maxCode = Long.parseLong(resultSet.getString("data"));
            } else {
                maxCode = 0l;
            }
            if (!resultSet.isBeforeFirst()) {
                System.out.println("No data");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return maxCode;
    }

    public Long findMaxPaymentCode(Connection connection) {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Long maxCode = 0l;
        try {
            connection = dbm.getConnection();
            preparedStatement = connection.prepareStatement("SELECT max(id) data FROM" + PAYMENT_TABLE);
            resultSet = preparedStatement.executeQuery();
            resultSet.next();
            if (resultSet.getString("data") != null && !resultSet.getString("data").isEmpty()) {
                maxCode = Long.parseLong(resultSet.getString("data"));
            } else {
                maxCode = 0l;
            }
            if (!resultSet.isBeforeFirst()) {
                System.out.println("No data");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return maxCode;
    }

    public Long findMaxUserCode(Connection connection) {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Long maxCode = 0l;
        try {
            connection = dbm.getConnection();
            preparedStatement = connection.prepareStatement("SELECT max(code) data FROM" + USERS_TABLE);
            resultSet = preparedStatement.executeQuery();
            resultSet.next();
            if (resultSet.getString("data") != null && !resultSet.getString("data").isEmpty()) {
                maxCode = Long.parseLong(resultSet.getString("data"));
            } else {
                maxCode = 0l;
            }
            if (!resultSet.isBeforeFirst()) {
                System.out.println("No data");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return maxCode;
    }

    public void insertOfficePrepareStatement() {
        Connection con = null;
        PreparedStatement preparedStatement = null;
//        String query = "insert into office (name, address,city) values(?,?,?)";
        String query = "select * from office";
        try {
            con = dbm.getConnection();
            preparedStatement = con.prepareStatement(query);
            long start = System.currentTimeMillis() / 1000;
//            preparedStatement.setString(1, "Name");
//            preparedStatement.setString(2, "Sabaneta ");
//            preparedStatement.setString(3, "Santiago");
//            preparedStatement.executeUpdate();
            preparedStatement.executeQuery();
            System.out.println(query);
            System.out.println("TimeTaken =         " + (System.currentTimeMillis() - start));
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void insertOfficeBatchAndPrepareStatement() {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "insert into office (name, address,city) values(?,?,?)";
        try {
            con = dbm.getConnection();
            ps = con.prepareStatement(query);
            long start = System.currentTimeMillis() / 1000;
            for (int i = 0; i < 10000; i++) {
                ps.setString(1, "Name" + i);
                ps.setString(2, "Sabaneta " + i);
                ps.setString(3, "Santiago" + i);
                System.out.println(ps);
                ps.addBatch();
                if (i % 1000 == 0) {
                    ps.executeBatch();
                }
            }
            ps.executeUpdate();
            System.out.println("TimeTaken " + (System.currentTimeMillis() - start));
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                ps.close();
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void saveCustomerPS(Customer customer) {
        Connection con = null;
        PreparedStatement preparedStatement = null;
        String findQuery = "select code from " + CUSTOMER_TABLE + " where code =" + customer.getCode();

        ResultSet resultSet = find(con, findQuery);
//        if (resultSet != null) {
//        }
        String query = "select * from office";
        try {
            con = dbm.getConnection();
            preparedStatement = con.prepareStatement(query);
//            preparedStatement.setString(1, "Name");
//            preparedStatement.setString(2, "Sabaneta ");
//            preparedStatement.setString(3, "Santiago");
//            preparedStatement.executeUpdate();
            preparedStatement.executeQuery();
            System.out.println(query);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void saveVehiclePS(Vehicle vehicle) {
        Connection con = null;
        PreparedStatement preparedStatement = null;
        con = dbm.getConnection();
        String findQuery = "select car_id from " + VEHICLE_TABLE + " where car_id = '" + vehicle.getCarID() + "' ;";
        String query = "";
        ResultSet resultSet = find(con, findQuery);
        if (resultSet != null && !resultSet.equals("")) {
            query = "insert into  " + VEHICLE_TABLE + " (car_id, brand, color, price, model_name"
                    + ", year, chassis, passager_number, vehicle_type) "
                    + "values(?,?,?,?,?,?,?,?,?)";
        } else {

        }
        try {
            preparedStatement = con.prepareStatement(query);
            preparedStatement.setString(1, vehicle.getCarID().toString());
            preparedStatement.setString(2, vehicle.getBrand());
            preparedStatement.setString(3, vehicle.getColor());
            preparedStatement.setString(4, vehicle.getPrice().toString());
            preparedStatement.setString(5, vehicle.getModelName());
            preparedStatement.setString(6, vehicle.getYear().toString());
            preparedStatement.setString(7, vehicle.getChassis());
            preparedStatement.setString(8, vehicle.getPassangerNumbers().toString());
            preparedStatement.setString(9, vehicle.getVehicleType());
            preparedStatement.executeUpdate();
            System.out.println(query);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void saveRentalPS(Rental rental) {
        Connection con = null;
        PreparedStatement preparedStatement = null;
        con = dbm.getConnection();
        String findQuery = "select id from " + RENTAL_TABLE + " where id = '" + rental.getId() + "' ;";
        String query = "";
        ResultSet resultSet = find(con, findQuery);
        if (resultSet != null && !resultSet.equals("")) {
            query = "insert into  " + RENTAL_TABLE + " (id, pickup_date, return_date, idVehicle, idPayment, description,"
                    + " odometer_start, odometer_end, gas_tank_level, insurance, rental_type)"
                    + "values(?,?,?,?,?,?,?,?,?,?,?)";
        } else {

        }
        try {
            preparedStatement = con.prepareStatement(query);
            preparedStatement.setString(1, rental.getId().toString());
            preparedStatement.setDate(2, new java.sql.Date(rental.getPickupDate().getTime()));
            preparedStatement.setDate(3, new java.sql.Date(rental.getReturnDate().getTime()));
            preparedStatement.setString(4, rental.getVehicle().getCarID().toString());
            preparedStatement.setString(5, rental.getPayment().getId().toString());
            if (rental.getDescription() == null && rental.getDescription().toString().isEmpty()) {
                preparedStatement.setString(6, rental.getDescription().toString());
            } else {
                preparedStatement.setString(6, "");
            }
            preparedStatement.setString(7, rental.getOdometerStart().toString());
            preparedStatement.setString(8, rental.getOdometerEnd().toString());
            preparedStatement.setString(9, rental.getGasTankLevel().toString());
            preparedStatement.setString(10, rental.getInsurance());
            preparedStatement.setString(11, rental.getRentalType());
            preparedStatement.executeUpdate();
            System.out.println(query);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void savePaymentPS(Payment payment) {
        Connection con = null;
        PreparedStatement preparedStatement = null;
        con = dbm.getConnection();
        String findQuery = "select id from " + PAYMENT_TABLE + " where id =" + payment.getId();
        String query = "";
        ResultSet resultSet = find(con, findQuery);
        if (resultSet != null && !resultSet.equals("")) {
            query = "insert into  " + PAYMENT_TABLE + " (id,date,amount,payment_type,estatus) "
                    + "values(?,?,?,?,?)";
        } else {

        }
        try {
            preparedStatement = con.prepareStatement(query);
            preparedStatement.setString(1, payment.getId().toString());
            preparedStatement.setDate(2, new java.sql.Date(payment.getDate().getTime()));
            preparedStatement.setString(3, payment.getAmount().toString());
            preparedStatement.setString(4, payment.getPaymentType().toString());
            preparedStatement.setString(5, payment.getEstatus());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void saveUserPS(User user) {
        Connection con = null;
        PreparedStatement preparedStatement = null;
        con = dbm.getConnection();
        String findQuery = "select code from " + USERS_TABLE + " where code =" + user.getCode();
        String query = "";
        ResultSet resultSet = find(con, findQuery);
        if (resultSet != null && !resultSet.equals("")) {
            query = "insert into  " + USERS_TABLE + " (code,name,username,password,address,idcard,email,gender,phone_number,user_type) "
                    + "values(?,?,?,?,?,?,?,?,?,?)";
        } else {

        }
        try {
            preparedStatement = con.prepareStatement(query);
            preparedStatement.setString(1, user.getCode().toString());
            preparedStatement.setString(2, user.getName());
            preparedStatement.setString(3, user.getUserName());
            preparedStatement.setString(4, user.getPassWord());
            preparedStatement.setString(5, user.getAddress());
            preparedStatement.setString(6, user.getIdCard());
            preparedStatement.setString(7, user.getEmail());
            preparedStatement.setString(8, user.getGender());
            preparedStatement.setString(9, user.getPhoneNumber());
            preparedStatement.setString(10, user.getUserType());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}

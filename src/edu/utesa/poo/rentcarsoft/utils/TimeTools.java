/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.utesa.poo.rentcarsoft.utils;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author ronald
 */
public class TimeTools {

    private static TimeTools instance = null;

    private TimeTools() {
    }

    public static synchronized TimeTools get() {
        if (instance == null) {
            instance = new TimeTools();
        }
        return instance;
    }
    public static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");

    public static Date addTime(Date date, int type, int cant) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(type, cant);
        return calendar.getTime();
    }

    public static Date addDay(Date date, int quoteNumber) {
        return addTime(date, Calendar.DAY_OF_MONTH, quoteNumber);
    }

    public static Date addWeek(Date date, int quoteNumber) {
        return addTime(date, Calendar.DAY_OF_MONTH, 7 * quoteNumber);
    }

    public static Date addBiweekly(Date date, int quoteNumber) {
        return addTime(date, Calendar.DAY_OF_MONTH, 15 * quoteNumber);
    }

    public static Date addMonth(Date date, int quoteNumber) {
        return addTime(date, Calendar.MONTH, quoteNumber);
    }

    private static Date addYear(Date date, int quoteNumber) {
        return addTime(date, Calendar.YEAR, quoteNumber);
    }

    /**
     * Find diference 2 date on days
     *
     * @param d1
     * @param d2
     * @return
     */
    public static long getDifferenceDays(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    /**
     * Find diference 2 date on hours
     *
     * @param d1
     * @param d2
     * @return
     */
    public static long getDifferenceHours(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS);
    }

    /**
     * Find diference 2 date on seconds
     *
     * @param d1
     * @param d2
     * @return
     */
    public static long getDifferenceSeconds(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.SECONDS.convert(diff, TimeUnit.MILLISECONDS);
    }

    /**
     * Compare a time with a date. On case Time is null get 0 for normalize
     *
     * Examples 1: time = 11:22:50 date = 11:26:36
     *
     * return -1
     *
     * Example 2: time = 11:22:50 date = 11:22:50 return 0
     *
     * Example 3: time = 14:22:50 date = 13:20:50 return 1
     *
     * @param time
     * @param date
     * @return -1 if time is less than date, 0 if are equals and 1 if time is
     * more than date
     */
    public static final int compareTimeOnDate(Time time, Date date) {
        if (time == null) {
            return 0;
        }
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar1.setTime(new Date(time.getTime()));
        calendar2.setTime(new Date(Time.valueOf(simpleDateFormat.format(date)).getTime()));
        return compareCalendars(calendar1, calendar2);
    }

    /**
     * Compare dates Only time case
     *
     * @param dateFormat
     * @param dateStart
     * @param dateEnd
     * @return -1 if time is less than date, 0 if are equals and 1 if time is
     * more than date
     */
    public static final int compareDateOnDate(String dateFormat, Date dateStart, Date dateEnd) {
        SimpleDateFormat formater = new SimpleDateFormat(dateFormat);
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar1.setTime(new Date(Time.valueOf(formater.format(dateStart)).getTime()));
        calendar2.setTime(new Date(Time.valueOf(formater.format(dateEnd)).getTime()));
        return calendar1.compareTo(calendar2);
    }

    public static final boolean equalsDate(String dateFormat, Date dateStart, Date dateEnd) {
        SimpleDateFormat formater = new SimpleDateFormat(dateFormat);
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        try {
            calendar1.setTime(formater.parse(formater.format(dateStart)));
        } catch (ParseException ex) {
            Logger.getLogger(TimeTools.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            calendar2.setTime(formater.parse(formater.format(dateEnd)));
        } catch (ParseException ex) {
            Logger.getLogger(TimeTools.class.getName()).log(Level.SEVERE, null, ex);
        }
        return calendar1.compareTo(calendar2) == 0;
    }

    /**
     * Get all Time Zone Available
     *
     * @return List with Time zone
     */
    public static final List<String> getAllTimeZone() {
        return Arrays.asList(TimeZone.getAvailableIDs());
    }

    /**
     * Get day on week for a date.
     *
     * @param date
     * @return Int value on 1..7
     */
    public static final int getWeekDayOnDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.DAY_OF_WEEK);
    }

    public static final boolean isDiferentDayOnDates(Date date1, Date date2) {
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar1.setTime(date1);
        calendar2.setTime(date2);
        return calendar1.get(Calendar.DAY_OF_MONTH) != calendar2.get(Calendar.DAY_OF_MONTH);
    }

    public static int compareCalendars(Calendar calendar1, Calendar calendar2) {
        return calendar1.compareTo(calendar2);
    }

}

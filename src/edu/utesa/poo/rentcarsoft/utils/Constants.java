/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.utesa.poo.rentcarsoft.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author ronald
 */
public class Constants {

    private static Constants instance = null;

    public static final String APPNAME = "RentCarSoft";
    public static final String fullDateFormat = "dd-MM-yyyy HH:mm:ss";
    public static final String simpleDateFormat = "dd-MM-yyyy";
    final SimpleDateFormat simpleDateFormatDate = new SimpleDateFormat(simpleDateFormat);

    public List<String> Genres = new ArrayList<>();
    public List<String> PaymentType = new ArrayList<>();
    public List<String> RentalType = new ArrayList<>();
    public List<String> UserType = new ArrayList<>();
    public List<String> VehicleType = new ArrayList<>();

    private Constants() {
    }

    public static synchronized Constants get() {
        if (instance == null) {
            instance = new Constants();
        }
        return instance;
    }

    public String fullDateFormat(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat(fullDateFormat);
        return formatter.format(date);
    }

    public String simpleDateFormat(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat(simpleDateFormat);
        return formatter.format(date);
    }

    public List<String> getGenres() {
        Genres.add("Hombre");
        Genres.add("Mujer");
        Genres.add("otro");
        return Genres;
    }

    public List<String> getPaymentType() {
        PaymentType.add("Cash");
        PaymentType.add("Credit");
        return PaymentType;
    }

    public List<String> getRentalType() {
        RentalType.add("Normal");
        RentalType.add("fija");
        return RentalType;
    }

    public List<String> getUserType() {
        UserType.add("Admin");
        UserType.add("Employee");
        UserType.add("Seller");
        UserType.add("Customer");
        return UserType;
    }

    public List<String> getVehicleType() {
        VehicleType.add("Carro");
        VehicleType.add("Autobus");
        VehicleType.add("jeep");
        VehicleType.add("jepeta");
        VehicleType.add("suburban");
        VehicleType.add("motocicleta");
        return VehicleType;
    }

}

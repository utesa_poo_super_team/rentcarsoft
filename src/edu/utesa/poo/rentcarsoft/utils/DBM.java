package edu.utesa.poo.rentcarsoft.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author scarl
 */
public class DBM {

    public final static String SERVER_IP = "localhost";
    public final static String SERVER_DB = "rentcarsoft";
    public final static String DB_USER = "rentcar";
    public final static String DB_PASS = "rent123car";

    public final static String DB_DRIVER_MSSQL = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    public final static String DB_PORT_MSSQL = "1433";
    public final static String DB_URL_MSSQL = "jdbc:sqlserver://" + SERVER_IP + ":" + DB_PORT_MSSQL + ";databaseName=" + SERVER_DB + ";user=" + DB_USER + ";password=" + DB_PASS + "";

    public final static String DB_DRIVER_MYSQL = "org.mariadb.jdbc.Driver";
    public final static String DB_PORT_MYSQL = "3306";
    public final static String DB_URL_MYSQL = "jdbc:mariadb://" + SERVER_IP + ":" + DB_PORT_MYSQL + "/" + SERVER_DB + "?rewriteBatchedStatements=true";

    Connection connection = null;

    public Connection getConnection() {
        try {
            Class.forName(DB_DRIVER_MYSQL);
            System.out.println(DB_URL_MYSQL);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DBM.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            connection = DriverManager.getConnection(DB_URL_MYSQL, DB_USER, DB_PASS);
        } catch (SQLException ex) {
            Logger.getLogger(DBM.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("DB Connection created successfully");
        return connection;
    }

    public void getConnectionOther() {
        try {
            Class.forName(DB_DRIVER_MYSQL);
            System.out.print("Connecting to SQL Server ... ");
            try (Connection connection = DriverManager.getConnection(DB_URL_MYSQL, DB_USER, DB_PASS)) {
                System.out.println("Done.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DBM.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void disconnect() {
        connection = null;
    }
}

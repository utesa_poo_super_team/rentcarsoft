package edu.utesa.poo.rentcarsoft.models.components.table;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 * @author PAB
 */
public abstract class ATableModel extends AbstractTableModel {

    private static final long serialVersionUID = 1L;

    public boolean cellEditable = false;
    public List dataArray = new ArrayList();
    public int[] columneditable = null;
    public ArrayList<String> columnIdentifiers;

    @Override
    public int getRowCount() {
        return this.dataArray.size();
    }

    @Override
    public int getColumnCount() {
        return this.columnIdentifiers.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return this.dataArray.get(rowIndex);
    }

    @Override
    public String getColumnName(int column) {
        return this.columnIdentifiers.get(column);
    }

    public ArrayList getColumnIdentifiers() {
        return this.columnIdentifiers;
    }

    public void removeRow(int row) {
        this.dataArray.remove(row);
        this.fireTableRowsDeleted(row, row);
    }

    public void addRow(Object data) {
        this.dataArray.add(data);
        this.fireTableDataChanged();
    }

    public Object getRow(int index) {
        return this.dataArray.get(index);
    }

    public List getData() {
        return this.dataArray;
    }

    public void setData(List dataArray) {
        this.dataArray = dataArray;
    }

    public void setDataNoCheck(List dataArray) {
        this.dataArray = dataArray;
    }
}

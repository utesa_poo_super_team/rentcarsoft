package edu.utesa.poo.rentcarsoft.models.components.combobox;

import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 *
 * @author PAB
 */
public class ComboboxModel extends AbstractListModel implements ComboBoxModel {

    private static final long serialVersionUID = 1L;

    private Object seleccionado;
    private List datos = new ArrayList();

    public void setData(List dats) {
        this.datos = dats;
    }

    @Override
    public void setSelectedItem(Object anItem) {
        this.seleccionado = anItem;
    }

    @Override
    public Object getSelectedItem() {
        return this.seleccionado;
    }

    @Override
    public int getSize() {
        return this.datos.size();
    }

    @Override
    public Object getElementAt(int index) {
        return this.datos.get(index);
    }
}

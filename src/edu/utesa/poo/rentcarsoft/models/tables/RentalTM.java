package edu.utesa.poo.rentcarsoft.models.tables;

import edu.utesa.poo.rentcarsoft.models.domain.rent.Rental;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author PAB
 */
public class RentalTM extends ATableModel {

    private String[] columnasNombres;

    public RentalTM() {
        headerDefault();
        initTable();
    }

    public RentalTM(String[] cn) {
        columnasNombres = cn;
        initTable();
    }

    private void headerDefault() {
        columnasNombres = new String[11];
        columnasNombres[0] = "ID";
        columnasNombres[1] = "Pickup Date";
        columnasNombres[2] = "Return Date";
        columnasNombres[3] = "Vehicle";
        columnasNombres[4] = "Payment";
        columnasNombres[5] = "Description";
        columnasNombres[6] = "Kilometer start";
        columnasNombres[7] = "Kilometer end";
        columnasNombres[8] = "Insurance";
        columnasNombres[9] = "Gas Tank Level";
        columnasNombres[10] = "Rental Type";
    }

    public void updateTable() {
        //dataArray = MDB.getInstancia().getListaObjectos();
        fireTableDataChanged();
    }

    private void initTable() {
        ArrayList header = new ArrayList();
        header.addAll(Arrays.asList(columnasNombres));
        columnIdentifiers = header;
        dataArray = new ArrayList();
        fireTableDataChanged();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Rental object = (Rental) dataArray.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return object.getId();
            case 1:
                return object.getPickupDate();
            case 2:
                return object.getReturnDate();
            case 3:
                return object.getVehicle();
            case 4:
                return object.getPayment();
            case 5:
                return object.getDescription();
            case 6:
                return object.getOdometerStart();
            case 7:
                return object.getOdometerEnd();
            case 8:
                return object.getInsurance();
            case 9:
                return object.getGasTankLevel();
            case 10:
                return object.getRentalType();
            default:
                return null;
        }
    }
}

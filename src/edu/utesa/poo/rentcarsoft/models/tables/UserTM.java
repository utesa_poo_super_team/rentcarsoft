package edu.utesa.poo.rentcarsoft.models.tables;

import edu.utesa.poo.rentcarsoft.models.domain.security.User;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author PAB
 */
public class UserTM extends ATableModel {

    private String[] columnasNombres;

    public UserTM() {
        headerDefault();
        initTable();
    }

    public UserTM(String[] cn) {
        columnasNombres = cn;
        initTable();
    }

    private void headerDefault() {
        columnasNombres = new String[10];
        columnasNombres[0] = "ID";
        columnasNombres[1] = "Name";
        columnasNombres[2] = "Address";
        columnasNombres[3] = "Identification";
        columnasNombres[4] = "Phone Number";
        columnasNombres[5] = "Email";
        columnasNombres[6] = "User Type";
        columnasNombres[7] = "Gender";
        columnasNombres[8] = "Username";
        columnasNombres[9] = "Password";
    }

    public void updateTable() {
        //dataArray = MDB.getInstancia().getListaObjectos();
        fireTableDataChanged();
    }

    private void initTable() {
        ArrayList header = new ArrayList();
        header.addAll(Arrays.asList(columnasNombres));
        columnIdentifiers = header;
        dataArray = new ArrayList();
        fireTableDataChanged();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        User object = (User) dataArray.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return object.getCode();
            case 1:
                return object.getName();
            case 2:
                return object.getAddress();
            case 3:
                return object.getIdCard();
            case 4:
                return object.getPhoneNumber();
            case 5:
                return object.getEmail();
            case 6:
                return object.getGender().toString();
            case 7:
                return object.getUserType();
            case 8:
                return object.getUserName();
            case 9:
                return object.getPassWord();
            default:
                return null;
        }
    }
}

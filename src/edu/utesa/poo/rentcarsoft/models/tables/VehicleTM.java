package edu.utesa.poo.rentcarsoft.models.tables;

import edu.utesa.poo.rentcarsoft.models.domain.vehicle.Vehicle;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author PAB
 */
public class VehicleTM extends ATableModel {

    private String[] columnasNombres;

    public VehicleTM() {
        headerDefault();
        initTable();
    }

    public VehicleTM(String[] cn) {
        columnasNombres = cn;
        initTable();
    }

    private void headerDefault() {
        columnasNombres = new String[9];
        columnasNombres[0] = "ID";
        columnasNombres[1] = "Brand";
        columnasNombres[2] = "Vehicle Type";
        columnasNombres[3] = "Color";
        columnasNombres[4] = "Price";
        columnasNombres[5] = "Chassis";
        columnasNombres[6] = "Model";
        columnasNombres[7] = "Year";
        columnasNombres[8] = "Passangers";
    }

    public void updateTable() {
        fireTableDataChanged();
    }

    private void initTable() {
        ArrayList header = new ArrayList();
        header.addAll(Arrays.asList(columnasNombres));
        columnIdentifiers = header;
        dataArray = new ArrayList();
        fireTableDataChanged();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Vehicle object = (Vehicle) dataArray.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return object.getCarID();
            case 1:
                return object.getBrand();
            case 2:
                return object.getVehicleType();
            case 3:
                return object.getColor();
            case 4:
                return object.getPrice();
            case 5:
                return object.getChassis();
            case 6:
                return object.getModelName();
            case 7:
                return object.getYear();
            case 8:
                return object.getPassangerNumbers();
            default:
                return null;
        }
    }
}

package edu.utesa.poo.rentcarsoft.models.tables;

import edu.utesa.poo.rentcarsoft.models.domain.payment.Payment;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author PAB
 */
public class PaymentTM extends ATableModel {

    private String[] columnasNombres;

    public PaymentTM() {
        headerDefault();
        initTable();
    }

    public PaymentTM(String[] cn) {
        columnasNombres = cn;
        initTable();
    }

    private void headerDefault() {
        columnasNombres = new String[5];
        columnasNombres[0] = "ID";
        columnasNombres[1] = "Date";
        columnasNombres[2] = "Amount";
        columnasNombres[3] = "Payment Type";
        columnasNombres[4] = "Estatus";
    }

    public void updateTable() {
        fireTableDataChanged();
    }

    private void initTable() {
        ArrayList header = new ArrayList();
        header.addAll(Arrays.asList(columnasNombres));
        columnIdentifiers = header;
        dataArray = new ArrayList();
        fireTableDataChanged();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Payment object = (Payment) dataArray.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return object.getId();
            case 1:
                return object.getDate();
            case 2:
                return object.getAmount();
            case 3:
                return object.getPaymentType();
            case 4:
                return object.getEstatus();
            default:
                return null;
        }
    }
}

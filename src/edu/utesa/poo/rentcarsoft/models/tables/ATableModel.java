package edu.utesa.poo.rentcarsoft.models.tables;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 * @author PAB
 */
public abstract class ATableModel extends AbstractTableModel {

    public boolean cellEditable = false;
    public List dataArray = new ArrayList();
    public int[] columneditable = null;
    public ArrayList<String> columnIdentifiers;

    @Override
    public int getRowCount() {
        return dataArray.size();
    }

    @Override
    public int getColumnCount() {
        return columnIdentifiers.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return dataArray.get(rowIndex);
    }

    @Override
    public String getColumnName(int column) {
        return columnIdentifiers.get(column);
    }

    public ArrayList getColumnIdentifiers() {
        return columnIdentifiers;
    }

    public void removeRow(int row) {
        dataArray.remove(row);
        fireTableRowsDeleted(row, row);
    }

    public void addRow(Object data) {
        this.dataArray.add(data);
        fireTableDataChanged();
    }

    public Object getRow(int index) {
        return dataArray.get(index);
    }

    public List getData() {
        return dataArray;
    }

    public void setData(List dataArray) {
        this.dataArray = dataArray;
    }

    public void setDataNoCheck(List dataArray) {
        this.dataArray = dataArray;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.utesa.poo.rentcarsoft.models.interfaces;

/**
 *
 * @author ronald
 */
public interface CrudInterface<T> {

    public T onCreate(T t);

    public T onSave(T t);

    public T onView(T t);

    public T onDelete(T t);

}

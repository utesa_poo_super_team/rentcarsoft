package edu.utesa.poo.rentcarsoft.models.domain.security;

/**
 *
 * @author ronald
 */
public class BaseUser {

    private Long code;
    private String name;
    private String address;
    private String idCard;
    private String phoneNumber;
    private String email;
    private String gender;
    private String userType;

    public BaseUser() {
    }

    public BaseUser(Long code, String name, String address, String idCard, String phoneNumber, String email, String gender, String userType) {
        this.code = code;
        this.name = name;
        this.address = address;
        this.idCard = idCard;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.gender = gender;
        this.userType = userType;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

}

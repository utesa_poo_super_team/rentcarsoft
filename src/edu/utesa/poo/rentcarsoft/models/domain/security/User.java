package edu.utesa.poo.rentcarsoft.models.domain.security;

import edu.utesa.poo.rentcarsoft.models.interfaces.CrudInterface;

/**
 *
 * @author ronald
 */
public class User extends BaseUser implements CrudInterface<User> {

    private String userName;
    private String passWord;

    public User() {
    }

    public User(String userName, String passWord, Long code, String name, String address,
            String idCard, String phoneNumber, String email, String gender, String userType) {
        super(code, name, address, idCard, phoneNumber, email, gender, userType);
        this.userName = userName;
        this.passWord = passWord;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    @Override
    public User onCreate(User t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public User onSave(User t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public User onView(User t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public User onDelete(User t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

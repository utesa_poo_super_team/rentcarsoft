package edu.utesa.poo.rentcarsoft.models.domain.payment;

import edu.utesa.poo.rentcarsoft.models.interfaces.CrudInterface;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author ronald
 */
public class Payment implements CrudInterface<Payment> {

    private Long id;
    private Date date;
    private BigDecimal amount;
    private String paymentType;
    private String estatus;

    public Payment() {
    }

    public Payment(Long id, Date date, BigDecimal amount, String paymentType, String estatus) {
        this.id = id;
        this.date = date;
        this.amount = amount;
        this.paymentType = paymentType;
        this.estatus = estatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    @Override
    public Payment onCreate(Payment t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Payment onSave(Payment t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Payment onView(Payment t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Payment onDelete(Payment t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return "id=" + id + ", date=" + date + ", amount=" + amount + '}';
    }

}

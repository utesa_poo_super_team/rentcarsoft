package edu.utesa.poo.rentcarsoft.models.domain.control;

import edu.utesa.poo.rentcarsoft.models.interfaces.CrudInterface;

/**
 *
 * @author ronald
 */
public class Office implements CrudInterface<Office> {

    private String name;
    private String address;
    private String city;
    private Customer customer;

    public Office(String name, String address, String city, Customer customer) {
        this.name = name;
        this.address = address;
        this.city = city;
        this.customer = customer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Customer getCustomers() {
        return customer;
    }

    public void setCustomers(Customer customers) {
        this.customer = customers;
    }

    @Override
    public Office onCreate(Office t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Office onSave(Office t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Office onView(Office t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Office onDelete(Office t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

package edu.utesa.poo.rentcarsoft.models.domain.control;

import edu.utesa.poo.rentcarsoft.models.interfaces.CrudInterface;
import edu.utesa.poo.rentcarsoft.models.domain.rent.Rental;
import edu.utesa.poo.rentcarsoft.models.domain.security.BaseUser;

/**
 *
 * @author ronald
 */
public class Customer extends BaseUser implements CrudInterface<Customer> {

    private String licenseId;
    private String occupation;
    private Rental rental;

    public Customer(String licenseId, String occupation, Rental rental, Long code, String name,
            String address, String idCard, String phoneNumber, String email, String gender, String userType) {
        super(code, name, address, idCard, phoneNumber, email, gender, userType);
        this.licenseId = licenseId;
        this.occupation = occupation;
        this.rental = rental;
    }

    public String getLicenseId() {
        return licenseId;
    }

    public void setLicenseId(String licenseId) {
        this.licenseId = licenseId;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public Rental getRental() {
        return rental;
    }

    public void setRentals(Rental rental) {
        this.rental = rental;
    }

    @Override
    public Customer onCreate(Customer t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Customer onSave(Customer t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Customer onView(Customer t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Customer onDelete(Customer t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

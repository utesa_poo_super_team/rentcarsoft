package edu.utesa.poo.rentcarsoft.models.domain.vehicle;

import edu.utesa.poo.rentcarsoft.models.interfaces.CrudInterface;
import java.math.BigDecimal;

/**
 *
 * @author ronald
 */
public class Vehicle implements CrudInterface<Vehicle> {

    private Long carID;
    private String brand;
    private String vehicleType;
    private String color;
    private BigDecimal price;
    private String chassis;
    private String modelName;
    private Integer year;
    private Integer passangerNumbers;

    public Vehicle() {
    }

    public Vehicle(Long carID, String brand, String vehicleType, String color, BigDecimal price, String chassis, String modelName, Integer year, Integer passangerNumbers) {
        this.carID = carID;
        this.brand = brand;
        this.vehicleType = vehicleType;
        this.color = color;
        this.price = price;
        this.chassis = chassis;
        this.modelName = modelName;
        this.year = year;
        this.passangerNumbers = passangerNumbers;
    }

    public Long getCarID() {
        return carID;
    }

    public void setCarID(Long carID) {
        this.carID = carID;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getChassis() {
        return chassis;
    }

    public void setChassis(String chassis) {
        this.chassis = chassis;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getPassangerNumbers() {
        return passangerNumbers;
    }

    public void setPassangerNumbers(Integer passangerNumbers) {
        this.passangerNumbers = passangerNumbers;
    }

    @Override
    public Vehicle onCreate(Vehicle t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Vehicle onSave(Vehicle t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Vehicle onView(Vehicle t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Vehicle onDelete(Vehicle t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return carID + "-" + brand;
    }

}

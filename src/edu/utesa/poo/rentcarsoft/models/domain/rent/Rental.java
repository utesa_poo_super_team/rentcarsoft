package edu.utesa.poo.rentcarsoft.models.domain.rent;

import edu.utesa.poo.rentcarsoft.models.interfaces.CrudInterface;
import edu.utesa.poo.rentcarsoft.models.domain.payment.Payment;
import edu.utesa.poo.rentcarsoft.models.domain.vehicle.Vehicle;
import java.util.Date;

/**
 *
 * @author ronald
 */
public class Rental extends BaseRent implements CrudInterface<Rental> {

    private String description;
    private Long odometerStart;
    private Long odometerEnd;
    private String insurance;
    private Long gasTankLevel;
    private String rentalType;

    public Rental() {
    }

    public Rental(Long id, String description, Long odometerStart, Long odometerEnd, String insurance, Long gasTankLevel, String rentalType, Date pickupDate, Date returnDate, Vehicle vehicle, Payment payment) {
        super(id, pickupDate, returnDate, vehicle, payment);
        this.description = description;
        this.odometerStart = odometerStart;
        this.odometerEnd = odometerEnd;
        this.insurance = insurance;
        this.gasTankLevel = gasTankLevel;
        this.rentalType = rentalType;
    }

    public String getRentalType() {
        return rentalType;
    }

    public void setRentalType(String rentalType) {
        this.rentalType = rentalType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getOdometerStart() {
        return odometerStart;
    }

    public void setOdometerStart(Long odometerStart) {
        this.odometerStart = odometerStart;
    }

    public Long getOdometerEnd() {
        return odometerEnd;
    }

    public void setOdometerEnd(Long odometerEnd) {
        this.odometerEnd = odometerEnd;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public Long getGasTankLevel() {
        return gasTankLevel;
    }

    public void setGasTankLevel(Long gasTankLevel) {
        this.gasTankLevel = gasTankLevel;
    }

    @Override
    public Rental onCreate(Rental t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Rental onSave(Rental t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Rental onView(Rental t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Rental onDelete(Rental t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

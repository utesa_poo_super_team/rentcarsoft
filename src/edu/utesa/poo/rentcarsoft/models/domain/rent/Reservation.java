package edu.utesa.poo.rentcarsoft.models.domain.rent;

import edu.utesa.poo.rentcarsoft.models.interfaces.CrudInterface;
import edu.utesa.poo.rentcarsoft.models.domain.payment.Payment;
import edu.utesa.poo.rentcarsoft.models.domain.vehicle.Vehicle;
import java.util.Date;

/**
 *
 * @author ronald
 */
public class Reservation extends BaseRent implements CrudInterface<Reservation> {

    private String estatus;

    public Reservation(Long id, Date pickupDate, Date returnDate, Vehicle vehicle, Payment payment) {
        super(id, pickupDate, returnDate, vehicle, payment);
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    @Override
    public Reservation onCreate(Reservation t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Reservation onSave(Reservation t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Reservation onView(Reservation t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Reservation onDelete(Reservation t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

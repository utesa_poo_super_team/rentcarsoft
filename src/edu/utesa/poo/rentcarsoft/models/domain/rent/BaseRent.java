package edu.utesa.poo.rentcarsoft.models.domain.rent;

import edu.utesa.poo.rentcarsoft.models.domain.payment.Payment;
import edu.utesa.poo.rentcarsoft.models.domain.vehicle.Vehicle;
import java.util.Date;

/**
 *
 * @author ronald
 */
public class BaseRent {

    private Long id;
    private Date pickupDate;
    private Date returnDate;
    private Vehicle vehicle;
    private Payment payment;

    public BaseRent() {
    }

    public BaseRent(Long id, Date pickupDate, Date returnDate, Vehicle vehicle, Payment payment) {
        this.id = id;
        this.pickupDate = pickupDate;
        this.returnDate = returnDate;
        this.vehicle = vehicle;
        this.payment = payment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(Date pickupDate) {
        this.pickupDate = pickupDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }
}
